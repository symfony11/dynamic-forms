<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210328085520 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_2D5B0234CCF9E01E');
        $this->addSql('CREATE TEMPORARY TABLE __temp__city AS SELECT id, departement_id, name, code FROM city');
        $this->addSql('DROP TABLE city');
        $this->addSql('CREATE TABLE city (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, departement_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, code INTEGER NOT NULL, CONSTRAINT FK_2D5B0234CCF9E01E FOREIGN KEY (departement_id) REFERENCES departement (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO city (id, departement_id, name, code) SELECT id, departement_id, name, code FROM __temp__city');
        $this->addSql('DROP TABLE __temp__city');
        $this->addSql('CREATE INDEX IDX_2D5B0234CCF9E01E ON city (departement_id)');
        $this->addSql('DROP INDEX IDX_C1765B6398260155');
        $this->addSql('CREATE TEMPORARY TABLE __temp__departement AS SELECT id, region_id, name, code FROM departement');
        $this->addSql('DROP TABLE departement');
        $this->addSql('CREATE TABLE departement (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, region_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, code INTEGER NOT NULL, CONSTRAINT FK_C1765B6398260155 FOREIGN KEY (region_id) REFERENCES region (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO departement (id, region_id, name, code) SELECT id, region_id, name, code FROM __temp__departement');
        $this->addSql('DROP TABLE __temp__departement');
        $this->addSql('CREATE INDEX IDX_C1765B6398260155 ON departement (region_id)');
        $this->addSql('DROP INDEX IDX_1BDA53C6A73F0036');
        $this->addSql('CREATE TEMPORARY TABLE __temp__medecin AS SELECT id, ville_id, name FROM medecin');
        $this->addSql('DROP TABLE medecin');
        $this->addSql('CREATE TABLE medecin (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, city_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_1BDA53C68BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO medecin (id, city_id, name) SELECT id, ville_id, name FROM __temp__medecin');
        $this->addSql('DROP TABLE __temp__medecin');
        $this->addSql('CREATE INDEX IDX_1BDA53C68BAC62AF ON medecin (city_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_2D5B0234CCF9E01E');
        $this->addSql('CREATE TEMPORARY TABLE __temp__city AS SELECT id, departement_id, name, code FROM city');
        $this->addSql('DROP TABLE city');
        $this->addSql('CREATE TABLE city (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, departement_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, code INTEGER NOT NULL)');
        $this->addSql('INSERT INTO city (id, departement_id, name, code) SELECT id, departement_id, name, code FROM __temp__city');
        $this->addSql('DROP TABLE __temp__city');
        $this->addSql('CREATE INDEX IDX_2D5B0234CCF9E01E ON city (departement_id)');
        $this->addSql('DROP INDEX IDX_C1765B6398260155');
        $this->addSql('CREATE TEMPORARY TABLE __temp__departement AS SELECT id, region_id, name, code FROM departement');
        $this->addSql('DROP TABLE departement');
        $this->addSql('CREATE TABLE departement (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, region_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, code INTEGER NOT NULL)');
        $this->addSql('INSERT INTO departement (id, region_id, name, code) SELECT id, region_id, name, code FROM __temp__departement');
        $this->addSql('DROP TABLE __temp__departement');
        $this->addSql('CREATE INDEX IDX_C1765B6398260155 ON departement (region_id)');
        $this->addSql('DROP INDEX IDX_1BDA53C68BAC62AF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__medecin AS SELECT id, city_id, name FROM medecin');
        $this->addSql('DROP TABLE medecin');
        $this->addSql('CREATE TABLE medecin (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, ville_id INTEGER DEFAULT NULL)');
        $this->addSql('INSERT INTO medecin (id, ville_id, name) SELECT id, city_id, name FROM __temp__medecin');
        $this->addSql('DROP TABLE __temp__medecin');
        $this->addSql('CREATE INDEX IDX_1BDA53C6A73F0036 ON medecin (ville_id)');
    }
}
