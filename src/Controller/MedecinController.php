<?php

namespace App\Controller;

use App\Repository\MedecinRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\MedecinType;

class MedecinController extends AbstractController
{
    /**
     * @var MedecinRepository
     */
    private $medecinRepo;

    public function __construct(MedecinRepository $medecinRepo)
    {
      $this->medecinRepo = $medecinRepo;
    }
    /**
     * @Route("/", name="homepage")
     */
    public function home(Request $request)
    {
        $form = $this->createForm(MedecinType::class);
        $form->handleRequest($request);

        $totalMedecins = $this->medecinRepo->getTotalCount();
        return $this->render('pages/home.html.twig', [
          'total_medecin' => $totalMedecins,
          'medecin_form' => $form->createView(),
        ]);
    }
}
