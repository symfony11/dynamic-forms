<?php

namespace App\Entity;

use App\Entity\City;
use App\Entity\Region;
use App\Repository\DepartementRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=DepartementRepository::class)
 */
class Departement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Region", inversedBy="departements")
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\City", mappedBy="departement")
     */
    private $cities;

    public function __construct()
    {
      $this->cities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function addCity(City $city)
    {
      $this->cities[] = $city;
      return $this;
    }

    public function removeCity(City $city)
    {
      $this->cities->removeElement($city);
      return $this;
    }

    public function getCities()
    {
      return $this->cities;
    }

    public function getRegion()
    {
      return $this->region;
    }

    public function setRegion(Region $region)
    {
      $this->region = $region;
      return $this;
    }
}
